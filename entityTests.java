package Tests;

import entity.Entity;
import main.GamePanel;

import java.awt.*;


public class entityTests {

    GamePanel gp;
    Graphics2D g2;


    private Entity entity;


    //checks if player receive damage

    public boolean PlayersDamageReceive(int attack, int defense){
        boolean damage_receive = false;
        int damage = attack - defense;
        if(damage>0){
            damage_receive = true;
        }
        return damage_receive;
    }

    //checks if monsters hp bar on

    public boolean MonsterHPBarON(boolean hpBarOn, int type){
        boolean barOn = false;
        if(type == 2 && hpBarOn == true){
            barOn = true;
        }
        return barOn;
    }

    //checks if monsters bar off after some time

    public boolean MonsterHPBarOFFonTime(int hpBarCounter, boolean hpBarOn, int type){
        if(type == 2 && hpBarOn == true){
            if(hpBarCounter > 600){
                hpBarOn = false;
            }
        }
        return hpBarOn;
    }

    //checks of invincibility is off

    public boolean InvincibilityOFF(boolean attacking){
        boolean invincible = false;
        if(attacking == true){
            invincible = true;
        }
        return invincible;
    }

    //checks if monster is dying

    public boolean Dying(int life){
        boolean dying = false;
        if(life == 0){
            dying = true;
        }
        return dying;
    }





}