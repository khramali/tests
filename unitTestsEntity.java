package Tests;

import entity.Entity;
import main.EventHandler;
import main.GamePanel;
import object.OBJ_Axe;
import object.OBJ_Potion_Red;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.awt.*;
import java.awt.event.KeyEvent;

import static org.junit.jupiter.api.Assertions.*;


//UNIT TESTS FOR ENTITY
public class unitTestsEntity extends entityTests {

    private Entity entity;
    private GamePanel gp;



    @BeforeEach
    public void setUp() {
        entity = new Entity(gp);
        gp = new GamePanel();
        boolean eventDone = false;
        // Set up any necessary dependencies or initial states
    }

    @Test
    public void testPlayersDamageReceive() {
        // Set up initial state
        int attack = 10;
        int defense = 5;
        // Verify the expected result
        Assertions.assertTrue(PlayersDamageReceive(attack, defense));
    }

    @Test
    public void testMonsterHPBarON() {
        // Set up initial state
        boolean hpBarOn = true;
        int type = 2;
        // Verify the expected result
        Assertions.assertTrue(MonsterHPBarON(hpBarOn, type));
    }

    @Test
    public void testMonsterHPBarOFFonTime() {
        // Set up initial state
        int hpBarCounter = 700;
        boolean hpBarOn = true;
        int type = 2;

        // Verify the expected result
        Assertions.assertFalse(MonsterHPBarOFFonTime(hpBarCounter, hpBarOn, type) );
    }

    @Test
    public void testInvincibilityOFF() {
        // Set up initial state
        boolean attacking = true;

        // Verify the expected result
        Assertions.assertTrue(InvincibilityOFF(attacking));
    }

    @Test
    public void testDying() {
        // Set up initial state
        int life = 0;
        // Verify the expected result
        Assertions.assertTrue(Dying(life));
    }



    //PROCESS TESTS GAMEPANEL

    //Test the game panel setup process
    @Test
    void testGamePanelSetupProcess() {
        // Create a new GamePanel object
        GamePanel gamePanel = new GamePanel();

        // Simulate the setup process
        gamePanel.setupGame();
        assertEquals(GamePanel.titleState, gamePanel.gameState);
        assertNotNull(gamePanel.tileM);
        assertNotNull(gamePanel.keyH);
        assertNotNull(gamePanel.music);
        assertNotNull(gamePanel.se);
        assertNotNull(gamePanel.cChecker);
        assertNotNull(gamePanel.aSetter);
        assertNotNull(gamePanel.ui);
        assertNotNull(gamePanel.eHandler);
        assertNotNull(gamePanel.config);
        assertNotNull(gamePanel.player);
        int width = gp.tileSize * gp.maxScreenCol;
        int height = gp.tileSize * gp.maxScreenRow;
        // Check other relevant properties and dependencies
        assertEquals(960, width);
        assertEquals(576, height);
        assertEquals(Color.BLACK, gamePanel.getBackground());
        assertTrue(gamePanel.isDoubleBuffered());
        assertTrue(gamePanel.isFocusable());
    }

    //Test the game retry process
    @Test
    void testGameRetryProcess() {
        // Create a new GamePanel object
        GamePanel gamePanel = new GamePanel();

        // Simulate the game retry process
        gamePanel.setupGame();
        gamePanel.retry();

        assertEquals(GamePanel.titleState, gamePanel.gameState);
        assertNotNull(gamePanel.tileM);
        assertNotNull(gamePanel.keyH);
        assertNotNull(gamePanel.music);
        assertNotNull(gamePanel.se);
        assertNotNull(gamePanel.cChecker);
        assertNotNull(gamePanel.aSetter);
        assertNotNull(gamePanel.ui);
        assertNotNull(gamePanel.eHandler);
        assertNotNull(gamePanel.config);
        assertNotNull(gamePanel.player);
    }


    //Test the game restart process
    @Test
    void testGameRestartProcess() {
        // Create a new GamePanel object
        GamePanel gamePanel = new GamePanel();

        // Simulate the game restart process
        gamePanel.setupGame();
        gamePanel.restart();

        assertEquals(GamePanel.titleState, gamePanel.gameState);
        assertNotNull(gamePanel.tileM);
        assertNotNull(gamePanel.keyH);
        assertNotNull(gamePanel.music);
        assertNotNull(gamePanel.se);
        assertNotNull(gamePanel.cChecker);
        assertNotNull(gamePanel.aSetter);
        assertNotNull(gamePanel.ui);
        assertNotNull(gamePanel.eHandler);
        assertNotNull(gamePanel.config);
        assertNotNull(gamePanel.player);
    }

    //Test the game update process
    @Test
    void testGameUpdateProcess() {
        GamePanel gamePanel = new GamePanel();

        // Set the game state to playState
        gamePanel.gameState = gamePanel.playState;

        // Simulate the game update process
        gamePanel.setupGame();
        gamePanel.update();

        assertEquals(gamePanel.playState, gamePanel.gameState);
    }

    //check game over state
    @Test
    void testGameOverState() {
        GamePanel gamePanel = new GamePanel();

        // Set the game state to playState
        gamePanel.gameState = gamePanel.gameOverState;

        assertEquals(gamePanel.gameOverState, gamePanel.gameState);
    }

    @Test
    void checkDefaultValues(){

        int x = 5;
        int y = 5;

        assertEquals(gp.tileSize*x, gp.player.worldX);
        assertEquals(gp.tileSize*y, gp.player.worldY);

        assertEquals(6, gp.player.life);
        assertEquals(6, gp.player.maxLife);

        assertEquals(1, gp.player.level);
        assertEquals(0, gp.player.exp);
        assertEquals(3, gp.player.nextLevelExp);
    }

    @Test
    void checkAddingAxe(){
        gp.obj[2] = new OBJ_Axe(gp);
        gp.player.pickUpObject(2);
        assertNull(gp.obj[2]);
        assertEquals(3, gp.player.inventory.size());
    }

    @Test
    void checkPlayerReceiveDamage(){

        gp.player.life = 6;
        gp.player.damageMonster(0,4);
        assertEquals(0, gp.monster[0].life);
        assertTrue(gp.monster[0].invincible);
        assertTrue(gp.monster[0].dying);

        assertFalse(gp.monster[1].dying);
        assertFalse(gp.monster[2].dying);

        assertEquals(1, gp.player.exp);
        assertEquals(1, gp.player.getDefense());

        entity.damagePlayer(5);
        assertEquals(2, gp.player.life);
        gp.player.damageMonster(1,4);
        gp.player.damageMonster(2,4);
        assertTrue(gp.monster[1].dying);
        assertTrue(gp.monster[2].dying);

        assertEquals(3, gp.player.exp);
        assertEquals(2, gp.player.level);
    }

    @Test
    void checkHealItems(){
        gp.player.inventory.add(new OBJ_Potion_Red(gp));
        gp.player.life = 2;

        new OBJ_Potion_Red(gp).use(gp.player);
        assertEquals(7, gp.player.life);
        gp.player.update();
        assertEquals(6, gp.player.life);
    }

    @Test
    public void checkLimitsPit(){
        gp.eHandler.damagePit(21,25, gp.dialogueState);
        assertFalse(gp.eHandler.isEventDone());
        gp.eHandler.damagePit(26,25, gp.dialogueState);
        assertFalse(gp.eHandler.isEventDone());


        gp.eHandler.damagePit(23,25, gp.dialogueState);
        assertTrue(gp.eHandler.isEventDone());
        gp.eHandler.damagePit(24,25, gp.dialogueState);
        assertTrue(gp.eHandler.isEventDone());
        gp.eHandler.damagePit(25,25, gp.dialogueState);
        assertTrue(gp.eHandler.isEventDone());
    }

    @Test
    public void checkLimitsHealingPool(){
        gp.eHandler.healingPool(31,31, gp.dialogueState);
        assertFalse(gp.eHandler.isEventDone());
        gp.eHandler.healingPool(31,36, gp.dialogueState);
        assertFalse(gp.eHandler.isEventDone());
        gp.eHandler.healingPool(35,31, gp.dialogueState);
        assertFalse(gp.eHandler.isEventDone());
        gp.eHandler.healingPool(35,36, gp.dialogueState);
        assertFalse(gp.eHandler.isEventDone());


        gp.eHandler.healingPool(32,32, gp.dialogueState);
        assertTrue(gp.eHandler.isEventDone());
        gp.eHandler.healingPool(33,33, gp.dialogueState);
        assertTrue(gp.eHandler.isEventDone());
    }


    @Test
    void checkEnterKeyReleased(){
        KeyEvent keyEvent = new KeyEvent(gp.keyH.mockComponent, KeyEvent.KEY_RELEASED,
                System.currentTimeMillis(), 0, KeyEvent.VK_ENTER, KeyEvent.CHAR_UNDEFINED);
        gp.keyH.keyReleased(keyEvent);
        assertFalse(gp.keyH.enterPressed);
    }

    @Test
    void checkUpKeyReleased(){
        KeyEvent keyEvent = new KeyEvent(gp.keyH.mockComponent, KeyEvent.KEY_RELEASED,
                System.currentTimeMillis(), 0, KeyEvent.VK_UP, KeyEvent.CHAR_UNDEFINED);
        gp.keyH.keyReleased(keyEvent);
        assertFalse(gp.keyH.upPressed);
    }

    @Test
    void checkDownKeyReleased(){
        KeyEvent keyEvent = new KeyEvent(gp.keyH.mockComponent, KeyEvent.KEY_RELEASED,
                System.currentTimeMillis(), 0, KeyEvent.VK_DOWN, KeyEvent.CHAR_UNDEFINED);
        gp.keyH.keyReleased(keyEvent);
        assertFalse(gp.keyH.downPressed);
    }

    @Test
    void checkLeftKeyReleased(){
        KeyEvent keyEvent = new KeyEvent(gp.keyH.mockComponent, KeyEvent.KEY_RELEASED,
                System.currentTimeMillis(), 0, KeyEvent.VK_LEFT, KeyEvent.CHAR_UNDEFINED);
        gp.keyH.keyReleased(keyEvent);
        assertFalse(gp.keyH.leftPressed);
    }

    @Test
    void checkRightKeyReleased(){
        KeyEvent keyEvent = new KeyEvent(gp.keyH.mockComponent, KeyEvent.KEY_RELEASED,
                System.currentTimeMillis(), 0, KeyEvent.VK_RIGHT, KeyEvent.CHAR_UNDEFINED);
        gp.keyH.keyReleased(keyEvent);
        assertFalse(gp.keyH.rightPressed);
    }

    @Test
    public void checkPitEvent(){
        int col = 23;
        int row = 25;

        gp.player.life = 10;
        gp.gameState = 3;
        gp.eHandler.damagePit(col,row, gp.dialogueState);

        assertEquals(9, gp.player.life);
        assertFalse(gp.eHandler.canTouchEvent);

        assertEquals(gp.dialogueState, gp.gameState);

        assertEquals("You fall into a pit!", gp.ui.currentDialogue);

    }

    @Test
    public void checkHealingPoolEvent(){
        int col = 32;
        int row = 32;

        gp.player.life = 1;
        gp.player.mana = 1;
        gp.eHandler.healingPool(col,row, gp.dialogueState);

        assertEquals(6, gp.player.life);
        assertEquals(4, gp.player.mana);
        assertEquals("You drink the water, your life has recovered" +
                "\n(the game has been saved)", gp.ui.currentDialogue);

    }

    @Test
    void checkTitleState(){
        gp.keyH.titleState(KeyEvent.VK_DOWN);
        assert gp.ui.commandNum ==1;
        gp.keyH.titleState(KeyEvent.VK_DOWN);
        assert  gp.ui.commandNum == 2;
        gp.keyH.titleState(KeyEvent.VK_DOWN);
        assert gp.ui.commandNum == 0;

        gp.keyH.titleState(KeyEvent.VK_W);
        assert gp.ui.commandNum == 2;
        gp.keyH.titleState(KeyEvent.VK_DOWN);
        assert gp.ui.commandNum == 0;

        gp.keyH.titleState(KeyEvent.VK_ENTER);
        assert gp.gameState == gp.playState;
    }

    @Test
    public void checkPauseState(){
        gp.keyH.playState(KeyEvent.VK_Q);
        assert gp.gameState == gp.pauseState;

        gp.keyH.pauseState(KeyEvent.VK_Q);
        assert gp.gameState == gp.playState;
    }

    @Test
    public void checkGameOverState(){
        gp = new GamePanel();
        entity = new Entity(gp);

        gp.player.life = 0;
        gp.player.update();
        assert gp.gameState == gp.gameOverState;
    }
}

