package Tests;

import entity.Entity;
import main.GamePanel;
import object.OBJ_Axe;
import object.OBJ_Potion_Red;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class GameTests {

    private GamePanel gamePanel;
    GamePanel gp;

    private Entity entity;

    @Before
    public void setup() {
        // Set up the calculator object
        GamePanel gamePanel = new GamePanel();
    }

    @Test
    public void CheckDefaultValues() {
        // Precondition: Define the input values
        int x = 5;
        int y = 5;
        int life = 6;
        int maxLife = 6;
        int level = 1;
        int exp = 0;
        int nextLevelExp = 3;

        // Step 1: Verify the result
        assertEquals(gp.tileSize*x, gp.player.worldX);
        assertEquals(gp.tileSize*y, gp.player.worldY);

        assertEquals(life, gp.player.life);
        assertEquals(maxLife, gp.player.maxLife);

        assertEquals(level, gp.player.level);
        assertEquals(exp, gp.player.exp);
        assertEquals(nextLevelExp, gp.player.nextLevelExp);
    }

    @Test
    public void CheckAddingAxe() {
        // Precondition: Define the input values
        gp.obj[2] = new OBJ_Axe(gp);

        // Step 1:
        gp.player.pickUpObject(2);

        // Step 2: Verify the result
        assertNull(gp.obj[2]);
        assertEquals(3, gp.player.inventory.size());
    }


    @Test
    public void CheckPlayerReceiveDamage() {
        // Precondition: Define the input values
        gp.player.life = 6;

        // Step 1:
        gp.player.damageMonster(0,4);

        entity.damagePlayer(5);

        gp.player.damageMonster(1,4);
        gp.player.damageMonster(2,4);

        // Step 2: Verify the result
        assertEquals(0, gp.monster[0].life);
        assertTrue(gp.monster[0].invincible);
        assertTrue(gp.monster[0].dying);
        assertFalse(gp.monster[1].dying);
        assertFalse(gp.monster[2].dying);
        assertEquals(1, gp.player.exp);
        assertEquals(1, gp.player.getDefense());

        assertEquals(2, gp.player.life);

        assertTrue(gp.monster[1].dying);
        assertTrue(gp.monster[2].dying);

        assertEquals(3, gp.player.exp);
        assertEquals(2, gp.player.level);
    }

    @Test
    public void CheckHealItems() {
        // Precondition: Define the input values
        gp.player.life = 2;

        // Step 1:
        gp.player.inventory.add(new OBJ_Potion_Red(gp));
        new OBJ_Potion_Red(gp).use(gp.player);
        gp.player.update();

        // Step 2: Verify the result
        assertEquals(6, gp.player.life);

    }
}